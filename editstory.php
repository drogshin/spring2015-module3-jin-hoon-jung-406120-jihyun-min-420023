<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<?php
	session_start(); 
	require 'database.php';

	if($_SESSION['token'] != isset($_POST['token'])) {
		die;
	}

	$title = $_SESSION['title'];
	$link = $_SESSION['link'];
	$summary = $_SESSION['summary'];
	$post_by = $_SESSION['user_name'];
	$story_id = $_POST['story_id'];

	$stmt = $mysqli->prepare("select title, link, summary, post_by from stories WHERE story_id=?");

// user can edit story that s/he uploaded
	$editc= sprintf("<form method='post' action='editstoryfield.php'>
		Title:<input type='text' name='updatetitle' value=''><br>
		Link:<input type='text' name='updatelink' value=''><br>
		Summary:<input type='text' name='updatesummary' value=''><br>
		<input type='submit' value='Edit'>
		<input type='hidden' name='token' value='$_SESSION[token]'/>
		</form>");
	printf($editc);

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$_SESSION['story_id'] = $story_id;
	$_SESSION['post_by'] = $post_by;

	$stmt->execute();
	$stmt->close();


// user can go back to homepage
	printf("<form action='News_site_user.php' method='POST'><input type='submit' name='back' value='Back to Homepage'/></form>");

	?>
</body>
</html>