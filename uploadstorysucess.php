<?php
session_start(); 
require 'database.php';

if($_SESSION['token'] != isset($_POST['token'])) {
		die;
	}

$title = $_POST['title'];
$link = $_POST['link'];
$summary = $_POST['summary'];
$post_by = $_SESSION['user_name'];

// user can upload stories
$stmt = $mysqli->prepare("insert into stories(title, link, summary, post_by) values (?,?,?,?)");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->bind_param('ssss', $title, $link, $summary, $post_by);
$stmt->execute();
$stmt->close();

$_SESSION['title'] = $title;
$_SESSION['link'] = $link;
$_SESSION['summary'] = $summary;

echo "Story Uploaded";
printf("<form action='News_site_user.php' method='POST'><input type='submit' name='back' value='Back to Homepage'/></form>");
printf("<form action='logout.php' method='POST'><input type='submit' name='logout' value='Logout'/></form>");
?>