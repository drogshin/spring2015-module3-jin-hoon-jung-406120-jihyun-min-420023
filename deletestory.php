<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<?php
	session_start(); 
	require 'database.php';

	if($_SESSION['token'] != isset($_POST['token'])) {
		die;
	}

	$username = $_SESSION['user_name'];
	$story_id = $_POST['story_id'];

// user can delete stories that s/he uploaded
	$stmt = $mysqli->prepare("delete from stories where story_id = '$story_id'");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->execute();
	$stmt->close();

// returns to homepage upon deleting story
	header('Location:News_site_user.php');
	?>
	</html>
</body>