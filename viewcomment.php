<!DOCTYPE html>
<html>
<head><title>View Comments</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="comment.css">
</head>

<body>
	<?php
	require 'database.php';
	$story_id = $_POST['story_id'];

	$stmt = $mysqli->prepare("select story_id, comment, post_by, comment_num, date from comments WHERE story_id=?");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('i', $story_id);
	$stmt->execute();
	$stmt->bind_result($story_id, $comment, $post_by, $comment_num, $date);
	echo "<ul>\n";

//user can view comments all stories
	while($stmt->fetch()){
		printf("\t <li> %s <br> %s <br> %s \n",
			htmlspecialchars($comment),
			htmlspecialchars($post_by),
			htmlspecialchars($date)
			);
	}
	echo "</ul>\n";
	$stmt->close();

	printf("<form action='News_site.php' method='POST'><input type='submit' name='back' value='Back to Homepage'/></form>");

	?>

</body>
</html>

