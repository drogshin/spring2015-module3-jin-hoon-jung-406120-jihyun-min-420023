<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<h1>The News</h1>
	<h2>Welcome</h2>

	<div class="comments">
		<?php

		require 'database.php';

		$username = $_SESSION['username'];

// allows user to change password
		printf("<form action='input_new_pw.php' method='POST'><input type='submit' name='change' value='Change Password'/></form>");

		$stmt = $mysqli->prepare("select story_id, title, post_by, summary from stories");
		
// lists all stories uploaded by specific user
		echo " Stories Uploaded by User";
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		$stmt->execute();
		$stmt->bind_result($story_id, $title, $post_by, $summary);
		echo "<ul>\n";	
		while($stmt->fetch()){
			if($username == $post_by){
				$view = sprintf("<form action='viewcomment_user.php' method='POST'>
					<input type='hidden' name='story_id' value='%s'/>
					<input type='submit' name='view' value='View Comments'/>
					<input type='hidden' name='token' value='$_SESSION[token]'/>
					</form>",$story_id);
				$delete = sprintf("<form action='deletestory.php' method='POST'>
					<input type='hidden' name='story_id' value='%s'/>
					<input type='submit' name='delete' value='Delete'/>
					<input type='hidden' name='token' value='$_SESSION[token]'/>
					</form>",$story_id);
				$edit = sprintf("<form action='editstory.php' method='POST'>
					<input type='hidden' name='story_id' value='%s'/>
					<input type='submit' name='edit' value='Edit'/>
					<input type='hidden' name='token' value='$_SESSION[token]'/>
					</form>",$story_id);
				printf("\t<li>Title:%s <br>Posted by: %s <br>Summary: %s <br> %s <br> %s <br> %s </li>\n",
					htmlspecialchars($title),
					htmlspecialchars($post_by),
					htmlspecialchars($summary),
					$view,
					$delete,
					$edit
					);
			}
		}
		echo "</ul>\n";
		$stmt->close();

//lists all comments posted by user on stories
		echo " Comments Uploaded by User";

		$stmt2 = $mysqli->prepare("select story_id, comment, post_by, date, comment_num from comments");
		$stmt2->execute();

		$stmt2->bind_result($story_id, $comment, $post_by, $date, $comment_num);
		echo "<ul>\n";
		while($stmt2->fetch()){
			if($username == $post_by){
				$delete = sprintf("<form action='delete.php' method='POST'>
					<input type='hidden' name='comment_num' value='%s'/>
					<input type='submit' name='delete' value='Delete'/>
					</form>",$comment_num);
				$edit = sprintf("<form action='editcomment.php' method='POST'>
					<input type='hidden' name='comment_num' value='%s'/>
					<input type='submit' name='edit' value='Edit'/>
					</form>",$comment_num);
				printf("\t <li>Story: %s <br> Comment: %s <br>Posted by: %s <br>Date: %s <br> %s <br> %s \n",
					htmlspecialchars($title),
					htmlspecialchars($comment),
					htmlspecialchars($post_by),
					htmlspecialchars($date),
					$delete,
					$edit
					);
			}
		}
		echo "</ul>\n";
		$stmt2->close();
		$_SESSION['username'] = $username;
		?>

	</div>

</body>
</html>