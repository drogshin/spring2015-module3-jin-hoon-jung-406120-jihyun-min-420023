<!DOCTYPE html>
<html>
<head><title>View Comments</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="user.css">
</head>

<body>
	<?php
	session_start(); 

	$username = $_SESSION['user_name'];
	$story_id = $_POST['story_id'];

// allows user to post comment on the story
	printf("<form method='POST' action='postcomment.php'>
		<input type='text' name='comment' value='Post Comment'>Enter your comments here...<br>
		<input type='submit' value='Submit'>
		<input type='hidden' name='token' value='$_SESSION[token]'/>
		<input type='reset' value='Reset'>
		</form>");

	require 'database.php';

	$stmt = $mysqli->prepare("select story_id, comment, post_by, comment_num, date from comments WHERE story_id=?");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('s', $story_id);
	$stmt->execute();
	$stmt->bind_result($story_id, $comment, $post_by,$comment_num, $date);
	
// lists all comments on the story and gives user the ability to edit and/or delete comments that s/he posted
	echo "<ul>\n";
	while($stmt->fetch()){
		if($username == $post_by){
			$delete = sprintf("<form action='delete.php' method='POST'>
				<input type='hidden' name='comment_num' value='%s'/>
				<input type='submit' name='delete' value='Delete'/>
				<input type='hidden' name='token' value='$_SESSION[token]'/>
				</form>",$comment_num);
			$edit = sprintf("<form action='editcomment.php' method='POST'>
				<input type='hidden' name='comment_num' value='%s'/>
				<input type='submit' name='edit' value='Edit'/>
				<input type='hidden' name='token' value='$_SESSION[token]'/>
				</form>",$comment_num);
			printf("\t <li> %s <br> %s <br> %s <br> %s <br> %s \n",
				htmlspecialchars($comment),
				htmlspecialchars($post_by),
				htmlspecialchars($date),
				$delete,
				$edit
				);
		}
		else{printf("\t <li> %s <br> %s <br> %s\n",
			htmlspecialchars($comment),
			htmlspecialchars($post_by),
			htmlspecialchars($date)
			);
	}
}
echo "</ul>\n";
$_SESSION['story_id']= $story_id;
$_SESSION['user_name'] = $username;
$stmt->close();

// user can go back to site or logout
printf("<form action='News_site_user.php' method='POST'><input type='submit' name='back' value='Back to Homepage'/></form>");
printf("<form action='logout.php' method='POST'><input type='submit' name='logout' value='Logout'/></form>");
?>

</body>


</html>

