<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>

	<?php
	session_start(); 
	require 'database.php';
	$username = $_SESSION['user_name'];

	$stmt = $mysqli->prepare("select story_id, title, link, summary, post_by, date from stories order by rand() limit 1");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->execute();
	$stmt->bind_result($story_id, $title, $link, $summary, $post_by, $date);
	echo "<ul>\n";
	while($stmt->fetch()){
		if($username == $post_by){
			$view = sprintf("<form action='viewcomment_user.php' method='POST'>
				<input type='hidden' name='story_id' value='%s'/>
				<input type='submit' name='view' value='View Comments'/> </form>",$story_id);
			$delete = sprintf("<form action='deletestory.php' method='POST'>
				<input type='hidden' name='story_id' value='%s'/>
				<input type='submit' name='delete' value='Delete'/>
				<input type='hidden' name='token' value='$_SESSION[token]'/>
				</form>",$story_id);
			$edit = sprintf("<form action='editstory.php' method='POST'>
				<input type='hidden' name='story_id' value='%s'/>
				<input type='submit' name='edit' value='Edit'/>
				<input type='hidden' name='token' value='$_SESSION[token]'/>
				</form>",$story_id);
			printf("\t<li>Title: %s <br> %s <br> Written by: %s <br> %s <br> %s <br> %s </li>\n",
				htmlspecialchars($title),
				htmlspecialchars($summary),
				htmlspecialchars($post_by),
				$view,
				$delete,
				$edit
				);
		}
// they can only view comments (not able to edit or delete stories) that others have uploaded
		else{
			$view = sprintf("<form action='viewcomment_user.php' method='POST'>
				<input type='hidden' name='story_id' value='%s'/>
				<input type='submit' name='view' value='View Comments'/></form>",$story_id);
			printf("\t<li>Title: %s <br> %s <br> Post by: %s <br> %s </li>\n",
				htmlspecialchars($title),
				htmlspecialchars($summary),
				htmlspecialchars($post_by),
				$view
				);

		}
	}

	echo "</ul>\n";
	$stmt->close();

	?>

</body>
</html>