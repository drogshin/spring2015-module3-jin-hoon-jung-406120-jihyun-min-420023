<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="user.css">
    <title> Almost interesting News </title>
</head>
<body>
    <h1>The News</h1>
    <h2>Welcome</h2>

    <?php
    require 'database.php';

// this is after the visitor has signed in (and hereafter referred to as user)
    $username = $_SESSION['user_name'];
    // $token = $_SESSION['token'];


// directs to user profile page where they can view stories they uploaded and comments they've written
    printf("<form action='profile.php' method='GET'><input type='submit' name='profile' value='User Profile'/></form>");

    $stmt = $mysqli->prepare("select story_id, title, post_by, summary from stories");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($story_id, $title, $post_by, $summary);

// shows random post - part of our creative portion
    $random = sprintf("<form action='random.php' method='POST'>
        <input type='hidden' name='random' value='%s'/>
        <input type='submit' name='random' value='Random Post'/> </form>",$story_id);
    printf("%s", $random);
    
// shows all news stories; users can edit and delete stories they've uploaded, and they can
// also view all stories
    echo "<ul>\n";
    while($stmt->fetch()){
        if($username == $post_by){
            $view = sprintf("<form action='viewcomment_user.php' method='POST'>
                <input type='hidden' name='story_id' value='%s'/>
                <input type='submit' name='view' value='View Comments'/> </form>",$story_id);
            $delete = sprintf("<form action='deletestory.php' method='POST'>
                <input type='hidden' name='story_id' value='%s'/>
                <input type='submit' name='delete' value='Delete'/>
                <input type='hidden' name='token' value='$_SESSION[token]'/>
                </form>",$story_id);
            $edit = sprintf("<form action='editstory.php' method='POST'>
                <input type='hidden' name='story_id' value='%s'/>
                <input type='submit' name='edit' value='Edit'/>
                <input type='hidden' name='token' value='$_SESSION[token]'/>
                </form>",$story_id);
            printf("\t<li>Title: %s <br> Post by: %s <br> %s <br> %s <br> %s <br> %s </li>\n",
                htmlspecialchars($title),
                htmlspecialchars($post_by),
                htmlspecialchars($summary),
                $view,
                $delete,
                $edit
                );
        }
// they can only view comments (not able to edit or delete stories) that others have uploaded
        else{
            $view = sprintf("<form action='viewcomment_user.php' method='POST'>
                <input type='hidden' name='story_id' value='%s'/>
                <input type='submit' name='view' value='View Comments'/></form>",$story_id);
            printf("\t<li>Title: %s <br>Post by: %s <br> %s <br> %s </li>\n",
                htmlspecialchars($title),
                htmlspecialchars($post_by),
                htmlspecialchars($summary),
                $view
                );

        }
    }

    echo "</ul>\n";

    // users can upload stories or logout
    printf("<form action='uploadstory.php' method='POST'>
        <input type='submit' name='uploadstory' value='Upload Story'/>
        <input type='hidden' name='token' value='$_SESSION[token]'/>
        </form>");
    printf("<form action='logout.php' method='POST'><input type='submit' name='logout' value='Logout'/>
        <input type='hidden' name='token' value='$_SESSION[token]'/>
        </form>");
    
    $_SESSION['username'] = $username;
    $_SESSION['story_id'] = $story_id;
    $stmt->close();

    ?>

</body>
</html>