<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<?php
	session_start(); 
	require 'database.php';

	//if($_SESSION['token'] != isset($_POST['token'])) {
	//	die;
	//}
	$username = trim($_SESSION['username']);
	$pw = $_POST['password'];
	$newpw = crypt($_POST['newpassword']);

	$stmt = $mysqli->prepare("select username, user_pw from user_information WHERE username = ?");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('s', $username);
	$stmt->execute();
	$stmt->bind_result($username, $user_pw);

// user can update existing password, but must first verify their current password
// if input does not match existing record, redirected to a different page
	while($stmt->fetch()){
		if(crypt($pw, $user_pw) == $user_pw){
			$stmt->close();
			$stmt2 = $mysqli->prepare("update user_information set user_pw='$newpw' where username = '$username'");
			$stmt2->bind_param('ss', $newpw, $username);
			$stmt2->execute();
			$stmt2->close();
			$_SESSION['username'] = $username;
			$_SESSION['pw'] = $pw;
			//$_SESSION['token'] = substr(md5(rand()),0,10);

			header('Location:profile.php');
		}
		else{
			header('Location:loginfail.html');
			$stmt->close();

		}
	}

	?>

</body>
</html>