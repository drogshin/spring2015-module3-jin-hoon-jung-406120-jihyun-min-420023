<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="style.css">
    <title> Almost interesting News </title>
</head>
<body>

    <!-- main page of user site -->
    <h1>The News</h1>
    <h2>Please enter your name and username</h2>

    <!-- login -->
    <form action="verify_user.php" method="post">
        Username: <input type="text" name="username" class="box"><br>
        Password: <input type="password" name="password" class="box"><br>
        <input type="submit" value="Submit"><br>
        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>"><br>
    </form>

    <br />

    <!-- register new user -->
    <form action="register.php" method="post">
        New Username: <input type="text" name="newuser"><br>
        New Password: <input type="password" name="newpassword"><br>
        <input type="submit" value="Register">
        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>"><br>
    </form>

    <?php
    require 'database.php';

    $stmt = $mysqli->prepare("select story_id, title, post_by, summary from stories");
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }

    $stmt->execute();
    $stmt->bind_result($story_id, $title, $post_by, $summary);

// shows random post - part of our creative portion

    $random = sprintf("<form action='random.php' method='POST'>
        <input type='hidden' name='random' value='%s'/>
        <input type='submit' name='random' value='Random Post'/> </form>",$story_id);
    printf("%s", $random);

// visitors can see all comments on every story
    echo "<ul>\n";
    while($stmt->fetch()){
        $view = sprintf("<form action='viewcomment.php' method='POST'><input type='hidden' name='story_id' value='%s'/><input type='submit' name='view' value='View Comments'/></form>",$story_id);
        printf("\t<li>%s <br> %s <br> %s <br> %s </li>\n",
            htmlspecialchars($title),
            htmlspecialchars($post_by),
            htmlspecialchars($summary),
            $view
            );
    }
    echo "</ul>\n";

    $stmt->close();
    ?>

</body>
</html>