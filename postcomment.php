<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<?php
	session_start(); 
	require 'database.php';

	if($_SESSION['token'] != isset($_POST['token'])) {
		die;
	}

	$story_id=$_SESSION['story_id'];
	$comment= $_POST['comment'];
	$post_by=$_SESSION['user_name'];


	$stmt = $mysqli->prepare("insert into comments(story_id, comment, post_by) values (?,?,?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('sss', $story_id, $comment, $post_by);
	$stmt->execute();

	$stmt->close();

	$_SESSION['story_id'] = $story_id;
	$_SESSION['user_name'] = $post_by;
	$_SESSION['comment'] = $comment;
	header('Location:commentsuccess.php');
	?>
</body>
</html>