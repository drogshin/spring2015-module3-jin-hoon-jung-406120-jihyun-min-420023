<?php
require 'database.php';
session_start();
$user_name = trim($_POST["username"]);
$password = $_POST["password"];

$stmt = $mysqli->prepare("select username, user_pw from user_information WHERE username = ?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('s', $user_name);
$stmt->execute();
$stmt->bind_result($username, $user_pw);

// checks to see that username and password matches
// if they do not match, visitor is notified
// if successful, redirected to the user's site
while($stmt->fetch()){
	if(crypt($password, $user_pw) == $user_pw){
		$_SESSION['user_name'] = $user_name;
		$_SESSION['token'] = substr(md5(rand()),0,10);

		header('Location: News_site_user.php');
	}
	else if (!isset($password)) { 
		header('Location:loginfail.html'); }

		else {
			header('Location: loginfail.html');
		}
	}
	$stmt->close();
	?>
