<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="user.css">
	<title> Almost interesting News </title>
</head>
<body>
	<?php
	require 'database.php';
	session_start();
	$newusername = trim($_POST["newuser"]);
	$newpassword = crypt($_POST["newpassword"]);

	$stmt = $mysqli->prepare("select username, user_pw from user_information WHERE username = ?");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
	//header('Location: News_site.php');
		exit;
	}
	$stmt->bind_param('s', $newusername);
	$stmt->execute();

	$stmt->bind_result($username, $user_pw);
	$taken = false;

	while($stmt->fetch()){
		$taken = true;
		break;
	}

	$stmt->close();

// checks to see if the username is already taken
	if($taken) {
		echo "try a new one";
	}

//adds new user to database
	else {
		$stmt2 = $mysqli->prepare("insert into user_information(username, user_pw) values (?,?)");
		if(!$stmt2){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		else {
			header('Location: News_site_user.php');
	// 	if($_SESSION['token'] !== $_POST['token']) {
	// 	die;
	// }
		}

		$stmt2->bind_param('ss', $newusername, $newpassword);
		$stmt2->execute();
	// $_SESSION['token'] = substr(md5(rand()),0,10);
		$stmt2->close();
	}
	?>
</body>
</html>